from os import listdir, system, remove, rename
from os.path import isfile, join, exists

class contiguatorGo:
	subjectFullPath = ''
	subjectFilenameList = []
	queryFullPath = ''
	queryFilenameList = []
	outputFullPath = ''
	outputFilenameList = []
	contiguatorPath = ''

	def __init__(self, subjectFullPath, queryFullPath, outputFullPath, contiguatorPath):
		# assigning class data
		self.subjectFullPath = subjectFullPath
		self.queryFullPath = queryFullPath
		self.outputFullPath = outputFullPath
		self.contiguatorPath = contiguatorPath
		# check if paths ends with '/'
		# if not, add '/' to path data
		if not subjectFullPath[-1] == '/':
			self.subjectFullPath += '/'
		if not queryFullPath[-1] == '/':
			self.queryFullPath += '/'
		if not outputFullPath[-1] == '/':
			self.outputFullPath += '/'
		if not contiguatorPath[-1] == '/':
			self.contiguatorPath += '/'
		# load list of subject and query filenames into class
		self.loadFilenameList()

	# ==== loadFilenameList METHOD ====
	# Initiate class with required parameters and load all filenames
	def loadFilenameList(self):
		# ---- VARIABLE EXPLANATION ----
		# @f - temporary variable to keep each filename while checking
		# ------------------------------
		# printing progress to terminal
		print '|'
		print '| Loading file list from Subject and Query input folders.'
		# list everything from @subjectFullPath and append to @ subjectFilenameList if it is a file
		self.subjectFilenameList = [ f for f in listdir(self.subjectFullPath) if isfile(join(self.subjectFullPath, f)) ]
		# list everything from @queryFullPath and append to @ queryFilenameList if it is a file
		self.queryFilenameList = [ f for f in listdir(self.queryFullPath) if isfile(join(self.queryFullPath, f)) ]

	# ==== runContiguator METHOD ====
	# run CONTIGuator using according to class parameters
	def runContiguator(self):
		# printing progress to terminal
		print '|'
		print '| Starting CONTIGuator.'

		for subject_filename in self.subjectFilenameList:
			# ---- VARIABLE EXPLANATION ----
			# @subject_filename - a full name with extension from @subjectFilenameList
			# @subject_formatted_name - name of subject file without file extension
			subject_formatted_name = subject_filename[:subject_filename.find('.')]

			for query_filename in self.queryFilenameList:
				# ---- VARIABLE EXPLANATION ----
				# @query_filename - a full name with extension from @queryFilenameList
				# @query_formatted_name - name of query file without file extension
				# @command - command string to be execute
				# ------------------------------
				# printing progress to terminal
				print '| |'
				print '| | Running CONTIGuator on Subject: %s and Query: %s' % (subject_filename, query_filename)

				query_formatted_name = query_filename[:query_filename.find('.')]

				command = 'python %sCONTIGuator.py -c %s -r %s' % ( self.contiguatorPath, \
												   					self.queryFullPath + query_filename, \
												   					self.subjectFullPath + subject_filename \
																  )
				system(command)
				# move and rename the output file
				print join(outputFullPath, subject_filename[: subject_filename.find('.')] + '--' + query_filename[: query_filename.find('.')])

				rename( join('Map_'+ subject_filename[: subject_filename.find('.')], 'MappedContigs.txt'), \
						join(outputFullPath, subject_filename[: subject_filename.find('.')] + '--' + query_filename[: query_filename.find('.')]))

		# printing progress to terminal
		print '|'
		print '| CONTIGuator operation completed.'

if __name__ == '__main__':

	subjectFullPath = '/vagrant/DATA/references/'
	queryFullPath = '/vagrant/DATA/1K_contigs/'
	outputFullPath = '/vagrant/progs/CONTIGuator_v2.7/test_out/'
	contiguatorPath = '/vagrant/progs/CONTIGuator_v2.7/'
	contiguator = contiguatorGo(subjectFullPath, queryFullPath, outputFullPath, contiguatorPath)
	contiguator.runContiguator()

	pass